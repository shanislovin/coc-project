(function(){

  $(document).ready(function(){
    this.graph = ForceGraph3D()(document.getElementById('3dcontainer'))
      .backgroundColor("#FFFFFF")
      .cameraPosition({z:120})
      .nodeColor((d) => {
        return "RGB(50,50," + 255/100 * d.mean + ")";
      })
      .nodeResolution(200)
      .nodeVal((d) => {
        int = d.mean / 100;
        return d.sDev/4;
      })
      .nodeOpacity(1)
      .nodeLabel((d) => {
        return "<p style='color:grey'>" + d.testTitle+ "</p>";
      })
      .linkWidth((d) => {
          return 1 + d.rValue*2;
          /*
          if(d.source === 1){
            return 2.0;
          */
      })
      .linkColor(function(d){
          if(d.rValue >= 0)
            return 'green';
          else
            return 'red';
      })
      .graphData(data);
  });


})();


let data = {
  "nodes": [
    {
      "id": 1,
      "testTitle": "ProgramTest",
      "sDev": 22,
      "mean": 46.92,
      "group": 1,
      "colour": " "
    },
    {
      "id": 2,
      "testTitle": "SentenceTest",
      "sDev": 5,
      "mean": 97.11,
      "group": 2,
      "colour": " "
    },
    {
      "id": 3,
      "testTitle": "PatternTest",
      "sDev": 15,
      "mean": 60.26,
      "group": 3,
      "colour": " "
    },
    {
      "id": 4,
      "testTitle": "VocabTest",
      "sDev": 22,
      "mean": 53.83,
      "group": 4,
      "colour": " "
    },
    {
      "id": 5,
      "testTitle": "AlgebraTest",
      "sDev": 18,
      "mean": 35.33,
      "group": 5,
      "colour": " "
    },
    {
      "id": 6,
      "testTitle": "DigitSpanTest",
      "sDev": 15,
      "mean": 70.95,
      "group": 6,
      "colour": " "
    },
    {
      "id": 7,
      "testTitle": "GrammarTest",
      "sDev": 11,
      "mean": 86.17,
      "group": 7,
      "colour": " "
    },
    {
      "id": 8,
      "testTitle": "ReasoningTest",
      "sDev": 17,
      "mean": 69.79,
      "group": 8,
      "colour": " "
    }
  ],
  "links": [
    {
      "source": 1,
      "target": 2,
      "pValue": 0,
      "rValue": 0.138800192
    },
    {
      "source": 1,
      "target": 3,
      "pValue": 0,
      "rValue": 0.303694794
    },
    {
      "source": 1,
      "target": 4,
      "pValue": 0,
      "rValue": 0.088600376
    },
    {
      "source": 1,
      "target": 5,
      "pValue": 0,
      "rValue": 0.058217086
    },
    {
      "source": 1,
      "target": 6,
      "pValue": 0,
      "rValue": 0.321853791
    },
    {
      "source": 1,
      "target": 7,
      "pValue": 0,
      "rValue": 0.157345114
    },
    {
      "source": 1,
      "target": 8,
      "pValue": 0,
      "rValue": 0.534484745
    },
    {
      "source": 2,
      "target": 3,
      "pValue": 0,
      "rValue": 0.163144753
    },
    {
      "source": 2,
      "target": 4,
      "pValue": 0,
      "rValue": 0.352865037
    },
    {
      "source": 2,
      "target": 5,
      "pValue": 0,
      "rValue": 0.065626787
    },
    {
      "source": 2,
      "target": 6,
      "pValue": 0,
      "rValue": 0.245718088
    },
    {
      "source": 2,
      "target": 7,
      "pValue": 0,
      "rValue": 0.236246182
    },
    {
      "source": 2,
      "target": 8,
      "pValue": 0,
      "rValue": -0.014877724
    },
    {
      "source": 3,
      "target": 4,
      "pValue": 0,
      "rValue": 0.057379281
    },
    {
      "source": 3,
      "target": 5,
      "pValue": 0,
      "rValue": 0.263856892
    },
    {
      "source": 3,
      "target": 6,
      "pValue": 0,
      "rValue": 0.056519279

    },
    {
      "source": 3,
      "target": 7,
      "pValue": 0,
      "rValue": 0.100113064
    },
    {
      "source": 3,
      "target": 8,
      "pValue": 0,
      "rValue": 0.169207017
    },
    {
      "source": 4,
      "target": 5,
      "pValue": 0,
      "rValue": 0.16925451
    },
    {
      "source": 4,
      "target": 6,
      "pValue": 0,
      "rValue": -0.185206484
    },
    {
      "source": 4,
      "target": 7,
      "pValue": 0,
      "rValue": 0.438371327
    },
    {
      "source": 4,
      "target": 8,
      "pValue": 0,
      "rValue": 0.231892879
    },
    {
      "source": 5,
      "target": 6,
      "pValue": 0,
      "rValue": -0.123861797
    },
    {
      "source": 5,
      "target": 7,
      "pValue": 0,
      "rValue": 0.011773905
    },
    {
      "source": 5,
      "target": 8,
      "pValue": 0,
      "rValue": 0.113452359
    },
    {
      "source": 6,
      "target": 7,
      "pValue": 0,
      "rValue": -0.012211891
    },
    {
      "source": 6,
      "target": 8,
      "pValue": 0,
      "rValue": 0.388831717
    },
    {
      "source": 7,
      "target": 8,
      "pValue": 0,
      "rValue": 0.072812088
    }
  ]
}
