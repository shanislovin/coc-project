data = {
  "nodes": [
    {
      "testTitle": "ProgramTest",
      "group": 1,
      "colour": " "
    },
    {
      "testTitle": "SentenceTest",
      "group": 2,
      "colour": " "
    },
    {
      "testTitle": "PatternTest",
      "group": 3,
      "colour": " "
    },
    {
      "testTitle": "VocabTest",
      "group": 4,
      "colour": " "
    },
    {
      "testTitle": "AlgebraTest",
      "group": 5,
      "colour": " "
    },
    {
      "testTitle": "DigitSpanTest",
      "group": 6,
      "colour": " "
    },
    {
      "testTitle": "GrammarTest",
      "group": 7,
      "colour": " "
    },
    {
      "testTitle": "ReasoningTest",
      "group": 8,
      "colour": " "
    }
  ],
  "links": [
    {
      "source": 1,
      "target": 2,
      "pValue": ,
      "rValue": 0.138800192
    },
    {
      "source": 1,
      "target": 3,
      "pValue": ,
      "rValue": 0.303694794
    },
    {
      "source": 1,
      "target": 4,
      "pValue": ,
      "rValue": 0.088600376
    },
    {
      "source": 1,
      "target": 5,
      "pValue": ,
      "rValue": 0.058217086
    },
    {
      "source": 1,
      "target": 6,
      "pValue": ,
      "rValue": 0.321853791
    },
    {
      "source": 1,
      "target": 7,
      "pValue": ,
      "rValue": 0.157345114
    },
    {
      "source": 1,
      "target": 8,
      "pValue": ,
      "rValue": 0.534484745
    },
    {
      "source": 2,
      "target": 3,
      "pValue": ,
      "rValue": 0.163144753
    },
    {
      "source": 2,
      "target": 4,
      "pValue": ,
      "rValue": 0.352865037
    },
    {
      "source": 2,
      "target": 5,
      "pValue": ,
      "rValue": 0.065626787
    },
    {
      "source": 2,
      "target": 6,
      "pValue": ,
      "rValue": 0.245718088
    },
    {
      "source": 2,
      "target": 7,
      "pValue": ,
      "rValue": 0.236246182
    },
    {
      "source": 2,
      "target": 8,
      "pValue": ,
      "rValue": -0.014877724
    },
    {
      "source": 3,
      "target": 4,
      "pValue": ,
      "rValue": 0.057379281
    },
    {
      "source": 3,
      "target": 5,
      "pValue": ,
      "rValue": 0.263856892
    },
    {
      "source": 3,
      "target": 6,
      "pValue": ,
      "rValue": 0.056519279

    },
    {
      "source": 3,
      "target": 7,
      "pValue": ,
      "rValue": 0.100113064
    },
    {
      "source": 3,
      "target": 8,
      "pValue": ,
      "rValue": 0.169207017
    },
    {
      "source": 4,
      "target": 5,
      "pValue": ,
      "rValue": 0.16925451
    },
    {
      "source": 4,
      "target": 6,
      "pValue": ,
      "rValue": -0.185206484
    },
    {
      "source": 4,
      "target": 7,
      "pValue": ,
      "rValue": 0.438371327
    },
    {
      "source": 4,
      "target": 8,
      "pValue": ,
      "rValue": 0.231892879
    },
    {
      "source": 5,
      "target": 6,
      "pValue": ,
      "rValue": -0.123861797
    },
    {
      "source": 5,
      "target": 7,
      "pValue": ,
      "rValue": 0.011773905
    },
    {
      "source": 5,
      "target": 8,
      "pValue": ,
      "rValue": 0.113452359
    },
    {
      "source": 6,
      "target": 7,
      "pValue": ,
      "rValue": -0.012211891
    },
    {
      "source": 6,
      "target": 8,
      "pValue": ,
      "rValue": 0.388831717
    },
    {
      "source": 7,
      "target": 8,
      "pValue": ,
      "rValue": 0.072812088
    }
  ]
}
